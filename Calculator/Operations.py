"""
Création et Test de la class du calculateur
"""
class Operation:
    """
    Classe définissant les opérations mathématiques habituelles
    """
    result = 0
    def addition(self, a_1, b_1):
        """
        Fonction d'addition de deux entiers a et b
        """
        if isinstance(a_1, int) and isinstance(b_1, int):
            self.result = a_1 + b_1
            print(self.result)
        else:
            print("ERROR Veuillez rentre un entier")

    def multiplication(self, a_2, b_2):
        """
        Fonction de multiplication de deux entiers a et b
        """
        if isinstance(a_2, int) and isinstance(b_2, int):
            self.result = a_2 * b_2
            print(self.result)
        else:
            print("ERROR Veuillez rentre un entier")

    def soustraction(self, a_3, b_3):
        """
        Fonction de soustraction de deux entiers a et b
        """
        if isinstance(a_3, int) and isinstance(b_3, int):
            self.result = a_3 - b_3
            print(self.result)
        else:
            print("ERROR Veuillez rentre un entier")

    def division(self, a_4, b_4):
        """
        Fonction de division de deux entiers a et b
        """
        try:
            if isinstance(a_4, int) and isinstance(b_4, int):
                self.result = a_4 / b_4
                print(self.result)
            else:
                print("ERROR Veuillez rentre un entier")
        except:
            raise ZeroDivisionError("Cannot divide by zero")




